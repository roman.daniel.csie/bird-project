Bird Watching API
=================

The Bird Watching API allows users to manage bird sightings and bird information, leveraging a PostgreSQL database for persistence. It supports operations to add, query, update, and delete bird and sighting information.

Technologies Used
-----------------

-   Spring Boot
-   PostgreSQL Database
-   Spring Data JPA
-   Docker

Prerequisites
-------------

-   JDK 11
-   Maven
-   Docker

Setup and Running the Application
---------------------------------

### Building the Project

To compile the project and package it into a runnable jar file, execute the following command in the root directory:

`mvn clean package`

This command compiles the project and produces a jar in the `target` directory.

### Running with Maven

To run the application directly using Maven:

`mvn spring-boot:run`

### Dockerizing the Application

#### Building Docker Image

Ensure Docker is installed and running on your system. Build the Docker image with:


`docker-compose build`

#### Running the Application in Docker

Start the application:

`docker-compose up`

### Accessing the Application

-   API Endpoint: The application is accessible at `http://localhost:8080`.

-   Database Access: The application uses PostgreSQL, which does not provide a built-in console like H2. To access the database, use PostgreSQL client tools such as `psql` or a GUI tool like pgAdmin. The default connection details are as follows:

    -   URL: `jdbc:postgresql://localhost:5432/bird_db`
    -   Username: `postgres`
    -   Password: `password`

    Ensure your PostgreSQL instance is properly set up as described in the Docker Compose configuration section if you are not using Docker Compose to run the application.
---------

### Birds

-   Add a Bird: `POST /api/birds`
-   Add Multiple Birds: `POST /api/birds/batch`
-   Get a Bird by GUID: `GET /api/birds/{guid}`
-   Get All Birds: `GET /api/birds`
-   Update a Bird: `PUT /api/birds/{guid}`
-   Delete a Bird: `DELETE /api/birds/{guid}`

### Bird Sightings

-   Add a Bird Sighting: `POST /api/bird-sightings`
-   Add Multiple Bird Sightings: `POST /api/bird-sightings/batch`
-   Get a Bird Sighting by GUID: `GET /api/bird-sightings/{guid}`
-   Get All Bird Sightings: `GET /api/bird-sightings`
-   Update a Bird Sighting: `PUT /api/bird-sightings/{guid}`
-   Delete a Bird Sighting: `DELETE /api/bird-sightings/{guid}`

### Query Endpoints

The API provides powerful query capabilities to retrieve birds and bird sightings based on specific criteria.

#### Query Birds by Name and Color

-   Endpoint: `POST /api/birds/query`
-   Description: Retrieves a list of birds that match the provided name and color criteria. Both parameters are optional and case-sensitive.
-   Request Body:
    -   `name`: (Optional) The name of the bird to filter by.
    -   `color`: (Optional) The color of the bird to filter by.
-   Example:

    `{
    "name": "Pitigoi",
    "color": "Gri"
    }`

-   Notes: If both `name` and `color` are omitted, the endpoint will return all birds. If either parameter is provided, it will return birds that match those specific criteria.

#### Query Bird Sightings

-   Endpoint: `POST /api/bird-sightings/query`
-   Description: Finds bird sightings based on the bird GUID, location, and a date/time range. All parameters are optional, offering flexibility in how sightings can be queried.
-   Request Body:
    -   `birdGuid`: (Optional) The unique identifier of the bird to find sightings for.
    -   `location`: (Optional) The location of the sighting.
    -   `startDate`: (Optional) The start of the date/time range for sightings.
    -   `endDate`: (Optional) The end of the date/time range for sightings.
-   Example:

    `{
    "birdGuid": "a3f1dec5-d5a7-41c7-8d3e-947b5c1dcbdb",
    "location": "Parcul Natural Vacaresti",
    "startDate": "2024-03-01T00:00:00",
    "endDate": "2024-03-31T23:59:59"
    }`

-   Flexibility:
    -   Date/Time Range: You can provide both `startDate` and `endDate` to get sightings within a specific period, just a `startDate` to get sightings from that date onwards, or just an `endDate` to get sightings up to that date.
    -   Location and Bird GUID: Can be used individually or combined with date/time criteria for more refined searches.

These query endpoints leverage the dynamic and powerful capabilities of JPA and Criteria API, allowing for complex searches with minimal effort. By using these endpoints, users can efficiently manage and access large datasets of birds and their sightings.

### Documentation

Access API documentation and try out the endpoints via Swagger UI at `http://localhost:8080/swagger-ui.html`

Stopping the Application
------------------------

To stop and remove containers:

`docker-compose down`