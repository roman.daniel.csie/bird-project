FROM openjdk:11

EXPOSE 8080

WORKDIR /app

COPY target/bird-project-*.jar app.jar

ENTRYPOINT ["java","-jar","app.jar"]