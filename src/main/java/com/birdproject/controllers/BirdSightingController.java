package com.birdproject.controllers;

import com.birdproject.model.dto.BirdSightingDTO;
import com.birdproject.model.requests.BirdSightingBatchRequest;
import com.birdproject.model.requests.BirdSightingQueryRequest;
import com.birdproject.model.requests.BirdSightingRequest;
import com.birdproject.services.birdSighting.BirdSightingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/bird-sightings")
@RestController
public class BirdSightingController {

    private final BirdSightingService birdSightingService;

    @Operation(summary = "Add a new bird sighting", description = "Records a new sighting of a bird with the provided details.")
    @ApiResponse(responseCode = "201", description = "Bird sighting added successfully.")
    @PostMapping
    public ResponseEntity<String> addSighting(@Valid @RequestBody BirdSightingRequest birdSightingRequest) {
        UUID guid = birdSightingService.saveSighting(birdSightingRequest);
        return new ResponseEntity<>("Bird sighting with guid: " + guid + " added successfully.", HttpStatus.CREATED);
    }

    @Operation(summary = "Add a batch of bird sightings", description = "Records multiple bird sightings.")
    @ApiResponse(responseCode = "201", description = "Bird sightings added successfully.")
    @PostMapping("/batch")
    public ResponseEntity<String> addSightingBatch(@Valid @RequestBody BirdSightingBatchRequest batchRequest) {
        List<UUID> savedGuids = birdSightingService.saveSightingBatch(batchRequest);
        return new ResponseEntity<>("Bird sightings added successfully. GUIDS: " + savedGuids, HttpStatus.CREATED);
    }

    @Operation(summary = "Get a bird sighting by GUID", description = "Retrieves the details of a bird sighting by its GUID.")
    @ApiResponse(responseCode = "200", description = "Bird sighting details retrieved successfully.")
    @GetMapping("/{guid}")
    public ResponseEntity<BirdSightingDTO> getSightingByGuid(@PathVariable UUID guid) {
        return new ResponseEntity<>(birdSightingService.getSightingByGuid(guid), HttpStatus.OK);
    }

    @Operation(summary = "Get all bird sightings", description = "Retrieves a list of all recorded bird sightings.")
    @ApiResponse(responseCode = "200", description = "All bird sightings listed successfully.")
    @GetMapping
    public ResponseEntity<List<BirdSightingDTO>> getAllSightings() {
        List<BirdSightingDTO> sightings = birdSightingService.getAllSightings();
        return new ResponseEntity<>(sightings, HttpStatus.OK);
    }

    @Operation(summary = "Update a bird sighting", description = "Updates the details of an existing bird sighting identified by its GUID.")
    @ApiResponse(responseCode = "200", description = "Bird sighting updated successfully.")
    @PutMapping("/{guid}")
    public ResponseEntity<String> updateSighting(@PathVariable UUID guid, @RequestBody BirdSightingRequest birdSightingRequest) {
        birdSightingService.updateSightingByGuid(guid, birdSightingRequest);
        return new ResponseEntity<>("Bird sighting with guid: " + guid + " updated successfully.", HttpStatus.OK);
    }

    @Operation(summary = "Delete a bird sighting", description = "Deletes a bird sighting identified by its GUID.")
    @ApiResponse(responseCode = "204", description = "Bird sighting deleted successfully.")
    @DeleteMapping("/{guid}")
    public ResponseEntity<Void> deleteSighting(@PathVariable UUID guid) {
        birdSightingService.deleteSighting(guid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Query bird sightings", description = "Finds bird sightings based on the provided criteria including bird GUID, location, and date/time range.")
    @ApiResponse(responseCode = "200", description = "Query executed successfully.")
    @PostMapping("/query")
    public ResponseEntity<List<BirdSightingDTO>> findSightingsByQuery(@RequestBody BirdSightingQueryRequest queryRequest) {
        List<BirdSightingDTO> sightings = birdSightingService.findSightingsByCriteria(queryRequest);
        return new ResponseEntity<>(sightings, HttpStatus.OK);
    }
}