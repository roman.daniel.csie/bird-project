package com.birdproject.controllers;

import com.birdproject.model.dto.BirdDTO;
import com.birdproject.model.requests.BirdBatchRequest;
import com.birdproject.model.requests.BirdQueryRequest;
import com.birdproject.model.requests.BirdRequest;
import com.birdproject.services.bird.BirdService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/birds")
@RestController
public class BirdController {

    private final BirdService birdService;

    @Operation(summary = "Add a new bird", description = "Creates a new bird entry with the provided details.")
    @ApiResponse(responseCode = "201", description = "Bird added successfully.")
    @PostMapping
    public ResponseEntity<String> addBird(@Valid @RequestBody BirdRequest addBirdRequest) {
        UUID guid = birdService.saveBird(addBirdRequest);
        return new ResponseEntity<>("Bird with guid: " + guid + " added successfully.", HttpStatus.CREATED);
    }

    @Operation(summary = "Add a batch of birds", description = "Creates multiple new bird entries.")
    @ApiResponse(responseCode = "201", description = "Birds added successfully.")
    @PostMapping("/batch")
    public ResponseEntity<String> addBirdBatch(@Valid @RequestBody BirdBatchRequest batchRequest) {
        List<UUID> savedGuids = birdService.saveBirdBatch(batchRequest);
        return new ResponseEntity<>("Birds added successfully. Guids: " + savedGuids, HttpStatus.CREATED);
    }

    @Operation(summary = "Get a bird by GUID", description = "Retrieves the details of a bird by its GUID.")
    @ApiResponse(responseCode = "200", description = "Bird details retrieved successfully.")
    @GetMapping("/{guid}")
    public ResponseEntity<BirdDTO> getBirdByGuid(@PathVariable UUID guid) {
        return new ResponseEntity<>(birdService.getBirdByGuid(guid), HttpStatus.OK);
    }

    @Operation(summary = "Get all birds", description = "Retrieves a list of all birds.")
    @ApiResponse(responseCode = "200", description = "All birds listed successfully.")
    @GetMapping
    public ResponseEntity<List<BirdDTO>> getAllBirds() {
        List<BirdDTO> birds = birdService.getAllBirds();
        return new ResponseEntity<>(birds, HttpStatus.OK);
    }

    @Operation(summary = "Update a bird", description = "Updates the details of an existing bird identified by its GUID.")
    @ApiResponse(responseCode = "200", description = "Bird updated successfully.")
    @PutMapping("/{guid}")
    public ResponseEntity<String> updateBird(@PathVariable UUID guid, @RequestBody BirdRequest updateBirdRequest) {
        birdService.updateBirdByGuid(guid, updateBirdRequest);
        return new ResponseEntity<>("Bird with guid: " + guid + " updated successfully.", HttpStatus.OK);
    }

    @Operation(summary = "Delete a bird", description = "Deletes a bird identified by its GUID.")
    @ApiResponse(responseCode = "204", description = "Bird deleted successfully.")
    @DeleteMapping("/{guid}")
    public ResponseEntity<Void> deleteBird(@PathVariable UUID guid) {
        birdService.deleteBird(guid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(summary = "Query birds by name and color", description = "Finds birds based on the provided name and color criteria.")
    @ApiResponse(responseCode = "200", description = "Query executed successfully.")
    @PostMapping("/query")
    public ResponseEntity<List<BirdDTO>> findBirdsByNameAndColor(@RequestBody BirdQueryRequest queryRequest) {
        List<BirdDTO> birds = birdService.findBirdsByNameAndColor(queryRequest);
        return new ResponseEntity<>(birds, HttpStatus.OK);
    }
}