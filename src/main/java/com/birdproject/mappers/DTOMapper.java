package com.birdproject.mappers;

import com.birdproject.exceptions.BirdNotFoundException;
import com.birdproject.model.db.Bird;
import com.birdproject.model.db.BirdSighting;
import com.birdproject.model.dto.BirdDTO;
import com.birdproject.model.dto.BirdSightingDTO;
import com.birdproject.repositories.BirdRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class DTOMapper {
    private final BirdRepository birdRepository;

    public BirdDTO toBirdDTO(Bird bird) {
        return BirdDTO.builder()
                .birdGuid(bird.getBirdGuid())
                .name(bird.getName())
                .color(bird.getColor())
                .weight(bird.getWeight())
                .height(bird.getHeight())
                .build();
    }

    public BirdSightingDTO toBirdSightingDTO(BirdSighting sighting) {
        val bird = birdRepository.findByBirdGuid(sighting.getBirdGuid())
                .orElseThrow(() -> new BirdNotFoundException("Bird with guid: " + sighting.getBirdGuid() + " not found"));
        return BirdSightingDTO.builder()
                .birdSightingGuid(sighting.getBirdSightingGuid())
                .bird(toBirdDTO(bird))
                .location(sighting.getLocation())
                .dateTime(sighting.getDateTime())
                .build();
    }
}
