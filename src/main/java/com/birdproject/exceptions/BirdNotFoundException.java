package com.birdproject.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BirdNotFoundException extends RuntimeException {
    public BirdNotFoundException(String message) {
        super(message);
    }
}