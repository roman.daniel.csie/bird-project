package com.birdproject.repositories;

import com.birdproject.model.db.Bird;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirdRepository extends JpaRepository<Bird, Long> {

    Optional<Bird> findByBirdGuid(UUID guid);

    @Query("SELECT b FROM Bird b WHERE (:name IS NULL OR b.name = :name) AND (:color IS NULL OR b.color = :color)")
    List<Bird> findByNameAndColor(@Param("name") String name, @Param("color") String color);
}