package com.birdproject.repositories;

import com.birdproject.model.db.BirdSighting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BirdSightingRepository extends JpaRepository<BirdSighting, Long> {

    Optional<BirdSighting> findByBirdSightingGuid(UUID guid);


    @Query(value = "SELECT s.* FROM bird_sightings s " +
            "WHERE (:birdGuid IS NULL OR s.bird_guid = CAST(:birdGuid AS UUID)) " +
            "AND (:location IS NULL OR s.location = :location) " +
            "AND (:startDate IS NULL OR s.date_time >= :startDate) " +
            "AND (:endDate IS NULL OR s.date_time <= :endDate)", nativeQuery = true)
    List<BirdSighting> findByCriteria(@Param("birdGuid") String birdGuid,
                                      @Param("location") String location,
                                      @Param("startDate") LocalDateTime startDate,
                                      @Param("endDate") LocalDateTime endDate);
}