package com.birdproject.services.birdSighting;

import com.birdproject.exceptions.BirdNotFoundException;
import com.birdproject.exceptions.BirdSightingNotFoundException;
import com.birdproject.mappers.DTOMapper;
import com.birdproject.model.db.BirdSighting;
import com.birdproject.model.dto.BirdSightingDTO;
import com.birdproject.model.requests.BirdSightingBatchRequest;
import com.birdproject.model.requests.BirdSightingQueryRequest;
import com.birdproject.model.requests.BirdSightingRequest;
import com.birdproject.repositories.BirdRepository;
import com.birdproject.repositories.BirdSightingRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BirdSightingServiceImpl implements BirdSightingService {

    private final BirdSightingRepository birdSightingRepository;
    private final BirdRepository birdRepository;
    private final DTOMapper dtoMapper;
    private final EntityManager entityManager;

    @Override
    @Transactional
    public UUID saveSighting(BirdSightingRequest birdSightingRequest) {
        if (birdRepository.findByBirdGuid(birdSightingRequest.getBirdGuid()).isEmpty()) {
            throw new BirdNotFoundException("Bird with guid: " + birdSightingRequest.getBirdGuid() + " not found");
        }
        val birdSighting = BirdSighting.builder()
                .birdSightingGuid(UUID.randomUUID())
                .birdGuid(birdSightingRequest.getBirdGuid())
                .location(birdSightingRequest.getLocation())
                .dateTime(birdSightingRequest.getDateTime())
                .build();

        birdSightingRepository.save(birdSighting);
        return birdSighting.getBirdSightingGuid();
    }

    @Override
    @Transactional
    public List<UUID> saveSightingBatch(BirdSightingBatchRequest batchRequest) {
        return batchRequest.getBirdSightings().stream()
                .map(this::saveSighting)
                .collect(Collectors.toList());
    }

    @Override
    public BirdSightingDTO getSightingByGuid(UUID guid) {
        val sighting = birdSightingRepository.findByBirdSightingGuid(guid);
        return dtoMapper.toBirdSightingDTO(sighting.orElseThrow(() ->
                new BirdSightingNotFoundException("Bird sighting with guid: " + guid + " not found")));
    }

    @Override
    public List<BirdSightingDTO> getAllSightings() {
        return birdSightingRepository.findAll().stream()
                .map(dtoMapper::toBirdSightingDTO).collect(Collectors.toList());
    }

    @Override
    public void updateSightingByGuid(UUID guid, BirdSightingRequest birdSightingRequest) {
        val sighting = birdSightingRepository.findByBirdSightingGuid(guid)
                .orElseThrow(() -> new BirdSightingNotFoundException("Bird sighting with guid: " + guid + " not found"));

        val birdGuid = birdSightingRequest.getBirdGuid() != null ? birdSightingRequest.getBirdGuid() : sighting.getBirdGuid();
        val location = birdSightingRequest.getLocation() != null ? birdSightingRequest.getLocation() : sighting.getLocation();
        val dateTime = birdSightingRequest.getDateTime() != null ? birdSightingRequest.getDateTime() : sighting.getDateTime();

        sighting.setBirdGuid(birdGuid);
        sighting.setLocation(location);
        sighting.setDateTime(dateTime);

        birdSightingRepository.save(sighting);
    }

    @Override
    public void deleteSighting(UUID guid) {
        val sighting = birdSightingRepository.findByBirdSightingGuid(guid)
                .orElseThrow(() -> new BirdSightingNotFoundException("Bird sighting with guid: " + guid + " not found"));

        birdSightingRepository.delete(sighting);
    }

    public List<BirdSightingDTO> findSightingsByCriteria(BirdSightingQueryRequest birdSightingQueryRequest) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<BirdSighting> cq = cb.createQuery(BirdSighting.class);

        Root<BirdSighting> sighting = cq.from(BirdSighting.class);
        List<Predicate> predicates = new ArrayList<>();

        if (birdSightingQueryRequest.getBirdGuid() != null) {
            predicates.add(cb.equal(sighting.get("birdGuid"), birdSightingQueryRequest.getBirdGuid()));
        }
        if (birdSightingQueryRequest.getLocation() != null) {
            predicates.add(cb.equal(sighting.get("location"), birdSightingQueryRequest.getLocation()));
        }

        if (birdSightingQueryRequest.getStartDate() != null && birdSightingQueryRequest.getEndDate() != null) {
            predicates.add(cb.between(sighting.get("dateTime"), birdSightingQueryRequest.getStartDate(), birdSightingQueryRequest.getEndDate()));
        } else if (birdSightingQueryRequest.getStartDate() != null) {
            predicates.add(cb.greaterThanOrEqualTo(sighting.get("dateTime"), birdSightingQueryRequest.getStartDate()));
        } else if (birdSightingQueryRequest.getEndDate() != null) {
            predicates.add(cb.lessThanOrEqualTo(sighting.get("dateTime"), birdSightingQueryRequest.getEndDate()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        List<BirdSighting> results = entityManager.createQuery(cq).getResultList();
        return results.stream()
                .map(dtoMapper::toBirdSightingDTO).collect(Collectors.toList());
    }
}