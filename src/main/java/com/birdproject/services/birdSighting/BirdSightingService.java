package com.birdproject.services.birdSighting;

import com.birdproject.model.db.BirdSighting;
import com.birdproject.model.dto.BirdSightingDTO;
import com.birdproject.model.requests.BirdSightingBatchRequest;
import com.birdproject.model.requests.BirdSightingQueryRequest;
import com.birdproject.model.requests.BirdSightingRequest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface BirdSightingService {
    UUID saveSighting(BirdSightingRequest birdSightingRequest);

    List<UUID> saveSightingBatch(BirdSightingBatchRequest batchRequest);

    BirdSightingDTO getSightingByGuid(UUID guid);

    List<BirdSightingDTO> getAllSightings();

    void updateSightingByGuid(UUID guid, BirdSightingRequest birdSightingRequest);

    void deleteSighting(UUID guid);

    List<BirdSightingDTO> findSightingsByCriteria(BirdSightingQueryRequest birdSightingQueryRequest);
}