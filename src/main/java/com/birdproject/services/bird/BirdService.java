package com.birdproject.services.bird;

import com.birdproject.model.dto.BirdDTO;
import com.birdproject.model.requests.BirdBatchRequest;
import com.birdproject.model.requests.BirdQueryRequest;
import com.birdproject.model.requests.BirdRequest;

import java.util.List;
import java.util.UUID;

public interface BirdService {
    UUID saveBird(BirdRequest addBirdRequest);

    List<UUID> saveBirdBatch(BirdBatchRequest batchRequest);

    BirdDTO getBirdByGuid(UUID guid);

    List<BirdDTO> getAllBirds();

    void updateBirdByGuid(UUID guid, BirdRequest updateBirdRequest);

    void deleteBird(UUID guid);

    List<BirdDTO> findBirdsByNameAndColor(BirdQueryRequest queryRequest);
}