package com.birdproject.services.bird;

import com.birdproject.exceptions.BirdNotFoundException;
import com.birdproject.mappers.DTOMapper;
import com.birdproject.model.db.Bird;
import com.birdproject.model.dto.BirdDTO;
import com.birdproject.model.requests.BirdBatchRequest;
import com.birdproject.model.requests.BirdQueryRequest;
import com.birdproject.model.requests.BirdRequest;
import com.birdproject.repositories.BirdRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BirdServiceImpl implements BirdService {

    private final BirdRepository birdRepository;
    private final DTOMapper dtoMapper;

    @Override
    @Transactional
    public UUID saveBird(BirdRequest addBirdRequest) {
        val bird = Bird.builder()
                .birdGuid(UUID.randomUUID())
                .name(addBirdRequest.getName())
                .color(addBirdRequest.getColor())
                .weight(addBirdRequest.getWeight())
                .height(addBirdRequest.getHeight())
                .build();

        birdRepository.save(bird);
        return bird.getBirdGuid();
    }

    @Override
    @Transactional
    public List<UUID> saveBirdBatch(BirdBatchRequest batchRequest) {
        return batchRequest.getBirds().stream()
                .map(this::saveBird)
                .collect(Collectors.toList());
    }

    @Override
    public BirdDTO getBirdByGuid(UUID guid) {
        val bird = birdRepository.findByBirdGuid(guid);
        return dtoMapper.toBirdDTO(bird.orElseThrow(() -> new BirdNotFoundException("Bird with guid: " + guid + " not found")));
    }

    @Override
    public List<BirdDTO> getAllBirds() {
        return birdRepository.findAll().stream()
                .map(dtoMapper::toBirdDTO).collect(Collectors.toList());
    }

    @Override
    public void updateBirdByGuid(UUID guid, BirdRequest updateBirdRequest) {
        val bird = birdRepository.findByBirdGuid(guid)
                .orElseThrow(() -> new BirdNotFoundException("Bird with guid: " + guid + " not found"));

        val name = updateBirdRequest.getName() != null ? updateBirdRequest.getName() : bird.getName();
        val color = updateBirdRequest.getColor() != null ? updateBirdRequest.getColor() : bird.getColor();
        val weight = updateBirdRequest.getWeight() != null ? updateBirdRequest.getWeight() : bird.getWeight();
        val height = updateBirdRequest.getHeight() != null ? updateBirdRequest.getHeight() : bird.getHeight();

        bird.setName(name);
        bird.setColor(color);
        bird.setWeight(weight);
        bird.setHeight(height);

        birdRepository.save(bird);
    }

    @Override
    public void deleteBird(UUID guid) {
        val bird = birdRepository.findByBirdGuid(guid)
                .orElseThrow(() -> new BirdNotFoundException("Bird with guid: " + guid + " not found"));

        birdRepository.delete(bird);
    }

    @Override
    public List<BirdDTO> findBirdsByNameAndColor(BirdQueryRequest queryRequest) {
        List<Bird> birds = birdRepository.findByNameAndColor(
                queryRequest.getName(), queryRequest.getColor());
        return birds.stream().map(dtoMapper::toBirdDTO).collect(Collectors.toList());
    }
}