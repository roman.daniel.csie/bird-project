package com.birdproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class BirdSightingDTO {
    private UUID birdSightingGuid;
    private BirdDTO bird;
    private String location;
    private LocalDateTime dateTime;
}