package com.birdproject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class BirdDTO {
    private UUID birdGuid;
    private String name;
    private String color;
    private Integer weight;
    private Integer height;
}