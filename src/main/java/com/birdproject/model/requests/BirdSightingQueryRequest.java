package com.birdproject.model.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirdSightingQueryRequest {
    private UUID birdGuid;
    private String location;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}