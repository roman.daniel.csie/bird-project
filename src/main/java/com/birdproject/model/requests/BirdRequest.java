package com.birdproject.model.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@JsonNaming(PropertyNamingStrategies.LowerCamelCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public class BirdRequest {
    @NotBlank(message = "Name must not be blank")
    @Size(max = 50, message = "Name must not exceed 50 characters")
    private String name;

    @NotBlank(message = "Color must not be blank")
    @Size(max = 50, message = "Color must not exceed 50 characters")
    private String color;

    @NotNull(message = "Weight must not be null")
    @Positive(message = "Weight must be positive")
    private Integer weight;

    @NotNull(message = "Height must not be null")
    @Positive(message = "Height must be positive")
    private Integer height;
}