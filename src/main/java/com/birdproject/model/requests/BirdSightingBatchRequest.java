package com.birdproject.model.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirdSightingBatchRequest {
    private List<BirdSightingRequest> birdSightings;
}