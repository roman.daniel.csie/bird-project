package com.birdproject.model.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirdSightingRequest {
    @NotNull(message = "Bird GUID must not be null")
    private UUID birdGuid;

    @NotBlank(message = "Location must not be blank")
    @Size(max = 50, message = "Location must not exceed 50 characters")
    private String location;

    @NotNull(message = "Date and time must not be null")
    private LocalDateTime dateTime;
}