package com.birdproject.model.error;

import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ApiError {
    private HttpStatus status;
    private String message;
    private Object errors;
}