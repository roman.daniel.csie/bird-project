package com.birdproject.test;

import com.birdproject.model.db.Bird;
import com.birdproject.model.db.BirdSighting;
import com.birdproject.model.requests.BirdRequest;
import com.birdproject.model.requests.BirdSightingQueryRequest;
import com.birdproject.model.requests.BirdSightingRequest;
import com.birdproject.repositories.BirdRepository;
import com.birdproject.repositories.BirdSightingRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BirdSightingControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private BirdSightingRepository birdSightingRepository;

    @Autowired
    private BirdRepository birdRepository;

    @AfterEach
    public void clearDatabase() {
        birdSightingRepository.deleteAll();
        birdRepository.deleteAll();
    }

    @Test
    public void addSighting_success() throws Exception {
        Bird bird = new Bird(null, UUID.randomUUID(), "Pitigoi", "Albastru", 250, 30);
        birdRepository.save(bird);

        BirdSightingRequest request = new BirdSightingRequest(
                bird.getBirdGuid(),
                "Vacaresti National Park",
                LocalDateTime.now());

        mockMvc.perform(post("/api/bird-sightings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated());

        List<BirdSighting> savedSightings = birdSightingRepository.findAll();
        assertThat(savedSightings).hasSize(1);

        BirdSighting savedSighting = savedSightings.get(0);
        assertThat(savedSighting.getLocation()).isEqualTo(request.getLocation());
    }

    @Test
    public void addBird_success() throws Exception {
        BirdRequest birdRequest = new BirdRequest("Robin", "Red", 150, 25);

        mockMvc.perform(post("/api/birds")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(birdRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    public void getAllBirds_success() throws Exception {
        mockMvc.perform(get("/api/birds")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(0))));
    }

    @Test
    public void getAllSightings_success() throws Exception {
        mockMvc.perform(get("/api/bird-sightings")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(0))));
    }

    @Test
    public void querySightingsByCriteria_success() throws Exception {
        BirdSightingQueryRequest queryRequest = new BirdSightingQueryRequest();
        queryRequest.setStartDate(LocalDateTime.now().minusDays(1));
        queryRequest.setEndDate(LocalDateTime.now());

        mockMvc.perform(post("/api/bird-sightings/query")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(queryRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(0))));
    }
}